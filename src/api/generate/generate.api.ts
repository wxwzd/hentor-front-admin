import { defHttp } from '/@/utils/http/axios';
import { Modal } from 'ant-design-vue';

enum Api {
  selectGenerateTable = '/generate/selectGenerateTable',
  selectGenerateTableDetail = '/generate/selectGenerateTableDetail',
  saveGenerateTable = '/generate/saveGenerateTable',
  updateGenerateTable = '/generate/updateGenerateTable',
  deleteGenerateTable = '/generate/deleteGenerateTable',
  syncDataBase = '/generate/syncDataBase',
  codeFileGenerator = '/generate/codeFileGenerator',
  downloadCodeZip = '/generate/downloadCodeZip',
}
export const selectGenerateTable = (params) =>
  defHttp.post({ url: Api.selectGenerateTable, params });

export const selectGenerateTableDetail = (params) =>
  defHttp.post({ url: Api.selectGenerateTableDetail, params });

export const saveGenerateTable = (params) => defHttp.post({ url: Api.saveGenerateTable, params });

export const updateGenerateTable = (params) =>
  defHttp.post({ url: Api.updateGenerateTable, params });

export const deleteGenerateTable = (params, handleSuccess) => {
  return defHttp.post({ url: Api.deleteGenerateTable, params }).then(() => {
    handleSuccess();
  });
};

export const syncDataBase = (params) => {
  return defHttp.post({ url: Api.syncDataBase, params });
};

export const codeFileGenerator = (params) => {
  return defHttp.post({ url: Api.codeFileGenerator, params });
};

export const downloadCodeZip = (params) => {
  return defHttp.post(
    { url: Api.downloadCodeZip, params, responseType: 'blob' },
    {
      isReturnNativeResponse: true,
    },
  );
};
