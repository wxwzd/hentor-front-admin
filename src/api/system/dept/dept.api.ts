import { DeptListItem, DeptListGetResultModel } from './deptModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  DeptList = '/sys/sysDepart/queryTreeList',
  add = '/sys/sysDepart/add',
  edit = '/sys/sysDepart/edit',
  delete = '/sys/sysDepart/delete',
  queryMyDeptTreeList = '/sys/sysDepart/queryMyDeptTreeList',
}

export const getDeptList = (params?: DeptListItem) =>
  defHttp.get<DeptListGetResultModel>({ url: Api.DeptList, params });

export const saveOrUpdateDept = (params?: DeptListItem, isUpdate?: boolean) => {
  const url = isUpdate ? Api.edit : Api.add;
  return defHttp.post({ url: url, params });
};

export const deleteDept = (params, handleSuccess) => {
  return defHttp.delete({ url: Api.delete, params }, { joinParamsToUrl: true }).then(() => {
    handleSuccess();
  });
};

export const queryMyDeptTreeList = (params?: DeptListItem) =>
  defHttp.get<DeptListGetResultModel>({ url: Api.queryMyDeptTreeList, params });
