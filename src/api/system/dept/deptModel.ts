import { BasicFetchResult } from '/@/api/model/baseModel';

export type DeptParams = {
  departName?: string;
  status?: string;
};

export interface DeptListItem {
  id: string;
  departOrder: string;
  createTime: string;
  remark: string;
  status: number;
  mobile: number;
  address: string;
}

/**
 * @description: Request list return value
 */

export type DeptListGetResultModel = BasicFetchResult<DeptListItem>;
