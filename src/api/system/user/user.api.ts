import { defHttp } from '/@/utils/http/axios';

enum Api {
  AccountList = '/sys/user/listAll',
  queryUserRole = '/sys/user/queryUserRole',
  checkOnlyUser = '/sys/user/checkOnlyUser',
  userDepartList = '/sys/user/userDepartList',
  add = '/sys/user/add',
  edit = '/sys/user/edit',
  delete = '/sys/user/delete',
  duplicateCheck = '/sys/user/check',
}

export const getAccountList = (params) => defHttp.get({ url: Api.AccountList, params });
export const queryUserRole = (params) =>
  defHttp.get({ url: Api.queryUserRole, params }, { joinParamsToUrl: true });

export const checkOnlyUser = (params) => defHttp.get({ url: Api.checkOnlyUser, params });

export const userDepartList = (params) =>
  defHttp.get({ url: Api.userDepartList, params }, { joinParamsToUrl: true });

export const saveOrUpdateUser = (params, isUpdate) => {
  if (isUpdate) {
    return defHttp.post({ url: Api.edit, params }).then(() => {});
  } else {
    return defHttp.post({ url: Api.add, params }).then(() => {});
  }
};

export const deleteUser = (params, handleSuccess) => {
  return defHttp.delete({ url: Api.delete, params }, { joinParamsToUrl: true }).then(() => {
    handleSuccess();
  });
};

/**
 * 唯一校验
 * @param params
 */
export const duplicateCheck = (params) =>
  defHttp.get({ url: Api.duplicateCheck, params }, { isTransformResponse: false });
