// import type { ProjectConfig } from '/#/config';
// import { MenuTypeEnum, MenuModeEnum, TriggerEnum, MixSidebarTriggerEnum } from '/@/enums/menuEnum';
// import { CacheTypeEnum } from '/@/enums/cacheEnum';
// import {
//   ContentEnum,
//   PermissionModeEnum,
//   ThemeEnum,
//   RouterTransitionEnum,
//   SettingButtonPositionEnum,
//   SessionTimeoutProcessingEnum,
// } from '/@/enums/appEnum';
// import { SIDE_BAR_BG_COLOR_LIST, HEADER_PRESET_BG_COLOR_LIST } from './designSetting';

// const primaryColor = '#0960bd';

// // ! You need to clear the browser cache after the change
// const setting: ProjectConfig = {
//   // Whether to show the configuration button
//   showSettingButton: true,

//   // Whether to show the theme switch button
//   showDarkModeToggle: true,

//   // `Settings` button position
//   settingButtonPosition: SettingButtonPositionEnum.AUTO,

//   // Permission mode
//   permissionMode: PermissionModeEnum.ROUTE_MAPPING,

//   // Permission-related cache is stored in sessionStorage or localStorage
//   permissionCacheType: CacheTypeEnum.LOCAL,

//   // Session timeout processing
//   sessionTimeoutProcessing: SessionTimeoutProcessingEnum.ROUTE_JUMP,

//   // color
//   themeColor: primaryColor,

//   // Website gray mode, open for possible mourning dates
//   grayMode: false,

//   // Color Weakness Mode
//   colorWeak: false,

//   // Whether to cancel the menu, the top, the multi-tab page display, for possible embedded in other systems
//   fullContent: false,

//   // content mode
//   contentMode: ContentEnum.FULL,

//   // Whether to display the logo
//   showLogo: true,

//   // Whether to show footer
//   showFooter: false,

//   // Header configuration
//   headerSetting: {
//     // header bg color
//     bgColor: HEADER_PRESET_BG_COLOR_LIST[0],
//     // Fixed at the top
//     fixed: true,
//     // Whether to show top
//     show: true,
//     // theme
//     theme: ThemeEnum.LIGHT,
//     // Whether to enable the lock screen function
//     useLockPage: true,
//     // Whether to show the full screen button
//     showFullScreen: true,
//     // Whether to show the document button
//     showDoc: true,
//     // Whether to show the notification button
//     showNotice: true,
//     // Whether to display the menu search
//     showSearch: true,
//   },

//   // Menu configuration
//   menuSetting: {
//     // sidebar menu bg color
//     bgColor: SIDE_BAR_BG_COLOR_LIST[0],
//     //  Whether to fix the left menu
//     fixed: true,
//     // Menu collapse
//     collapsed: false,
//     // When sider hide because of the responsive layout
//     siderHidden: false,
//     // Whether to display the menu name when folding the menu
//     collapsedShowTitle: false,
//     // Whether it can be dragged
//     // Only limited to the opening of the left menu, the mouse has a drag bar on the right side of the menu
//     canDrag: false,
//     // Whether to show no dom
//     show: true,
//     // Whether to show dom
//     hidden: false,
//     // Menu width
//     menuWidth: 210,
//     // Menu mode
//     mode: MenuModeEnum.INLINE,
//     // Menu type
//     type: MenuTypeEnum.SIDEBAR,
//     // Menu theme
//     theme: ThemeEnum.DARK,
//     // Split menu
//     split: false,
//     // Top menu layout
//     topMenuAlign: 'center',
//     // Fold trigger position
//     trigger: TriggerEnum.HEADER,
//     // Turn on accordion mode, only show a menu
//     accordion: true,
//     // Switch page to close menu
//     closeMixSidebarOnChange: false,
//     // Module opening method ‘click’ |'hover'
//     mixSideTrigger: MixSidebarTriggerEnum.CLICK,
//     // Fixed expanded menu
//     mixSideFixed: false,
//   },

//   // Multi-label
//   multiTabsSetting: {
//     cache: false,
//     // Turn on
//     show: true,
//     // Is it possible to drag and drop sorting tabs
//     canDrag: true,
//     // Turn on quick actions
//     showQuick: true,
//     // Whether to show the refresh button
//     showRedo: true,
//     // Whether to show the collapse button
//     showFold: true,
//   },

//   // Transition Setting
//   transitionSetting: {
//     //  Whether to open the page switching animation
//     // The disabled state will also disable pageLoading
//     enable: true,

//     // Route basic switching animation
//     basicTransition: RouterTransitionEnum.FADE_SIDE,

//     // Whether to open page switching loading
//     // Only open when enable=true
//     openPageLoading: true,

//     // Whether to open the top progress bar
//     openNProgress: false,
//   },

//   // Whether to enable KeepAlive cache is best to close during development, otherwise the cache needs to be cleared every time
//   openKeepAlive: true,

//   // Automatic screen lock time, 0 does not lock the screen. Unit minute default 0
//   lockTime: 0,

//   // Whether to show breadcrumbs
//   showBreadCrumb: true,

//   // Whether to show the breadcrumb icon
//   showBreadCrumbIcon: false,

//   // Use error-handler-plugin
//   useErrorHandle: false,

//   // Whether to open back to top
//   useOpenBackTop: true,

//   //  Is it possible to embed iframe pages
//   canEmbedIFramePage: true,

//   // Whether to delete unclosed messages and notify when switching the interface
//   closeMessageOnSwitch: true,

//   // Whether to cancel the http request that has been sent but not responded when switching the interface.
//   // If it is enabled, I want to overwrite a single interface. Can be set in a separate interface
//   removeAllHttpPending: false,
// };

// export default setting;
import type { ProjectConfig } from '/#/config';
import { MenuTypeEnum, MenuModeEnum, TriggerEnum, MixSidebarTriggerEnum } from '/@/enums/menuEnum';
import { CacheTypeEnum } from '/@/enums/cacheEnum';
import {
  ContentEnum,
  PermissionModeEnum,
  ThemeEnum,
  RouterTransitionEnum,
  SettingButtonPositionEnum,
  SessionTimeoutProcessingEnum,
  TabsThemeEnum,
} from '/@/enums/appEnum';
import { SIDE_BAR_BG_COLOR_LIST, HEADER_PRESET_BG_COLOR_LIST } from './designSetting';

const primaryColor = '#0960bd';
// ! 改动后需要清空浏览器缓存
const setting: ProjectConfig = {
  // 是否显示SettingButton
  showSettingButton: true,

  // 是否显示主题切换按钮
  showDarkModeToggle: true,

  // 设置按钮位置 可选项
  // SettingButtonPositionEnum.AUTO: 自动选择
  // SettingButtonPositionEnum.HEADER: 位于头部
  // SettingButtonPositionEnum.FIXED: 固定在右侧
  settingButtonPosition: SettingButtonPositionEnum.AUTO,

  // 权限模式,默认前端角色权限模式
  // ROUTE_MAPPING: 前端模式（菜单由路由生成，默认）
  // ROLE：前端模式（菜单路由分开）
  // BACK：后台模式
  permissionMode: PermissionModeEnum.BACK,

  // 权限缓存存放位置。默认存放于localStorage
  permissionCacheType: CacheTypeEnum.LOCAL,

  // 会话超时处理方案
  // SessionTimeoutProcessingEnum.ROUTE_JUMP: 路由跳转到登录页
  // SessionTimeoutProcessingEnum.PAGE_COVERAGE: 生成登录弹窗，覆盖当前页面
  sessionTimeoutProcessing: SessionTimeoutProcessingEnum.ROUTE_JUMP,

  // 项目主题色
  themeColor: primaryColor,

  // 网站灰色模式，用于可能悼念的日期开启
  grayMode: false,

  // 色弱模式
  colorWeak: false,

  // 是否取消菜单,顶部,多标签页显示, 用于可能内嵌在别的系统内
  fullContent: false,

  // 主题内容宽度
  contentMode: ContentEnum.FULL,

  // 是否显示logo
  showLogo: true,

  // 是否显示底部信息 copyright
  showFooter: false,

  // 头部配置
  headerSetting: {
    // 背景色
    bgColor: HEADER_PRESET_BG_COLOR_LIST[0],
    // 固定头部
    fixed: true,
    // 是否显示顶部
    show: true,
    // 主题
    theme: ThemeEnum.LIGHT,
    // 开启锁屏功能
    useLockPage: true,
    // 显示全屏按钮
    showFullScreen: true,
    // 显示文档按钮
    showDoc: true,
    // 显示消息中心按钮
    showNotice: true,
    // 显示菜单搜索按钮
    showSearch: true,
  },

  // 菜单配置
  menuSetting: {
    // 背景色
    bgColor: SIDE_BAR_BG_COLOR_LIST[0],
    // 是否固定住左侧菜单
    fixed: true,
    // 菜单折叠
    collapsed: false,
    // 折叠菜单时候是否显示菜单名
    collapsedShowTitle: false,
    // 是否可拖拽
    // Only limited to the opening of the left menu, the mouse has a drag bar on the right side of the menu
    canDrag: false,
    // Whether to show no dom
    show: true,
    // Whether to show dom
    hidden: false,
    // 菜单宽度
    menuWidth: 210,
    // 菜单模式
    mode: MenuModeEnum.INLINE,
    // 菜单类型
    type: MenuTypeEnum.SIDEBAR,
    // 菜单主题
    theme: ThemeEnum.DARK,
    // 分割菜单
    split: false,
    // 顶部菜单布局
    topMenuAlign: 'center',
    // 折叠触发器的位置
    trigger: TriggerEnum.HEADER,
    // 手风琴模式，只展示一个菜单
    accordion: true,
    // 在路由切换的时候关闭左侧混合菜单展开菜单
    closeMixSidebarOnChange: false,
    // 左侧混合菜单模块切换触发方式 ‘click’ |'hover'
    mixSideTrigger: MixSidebarTriggerEnum.CLICK,
    // 是否固定左侧混合菜单
    mixSideFixed: false,
  },

  // 多标签
  multiTabsSetting: {
    // 刷新后是否保留已经打开的标签页
    cache: false,
    // 开启
    show: true,
    // 是否可以拖拽
    canDrag: true,
    // 开启快速操作
    showQuick: true,
    // 是否显示刷新按钮
    showRedo: true,
    // 是否显示折叠按钮
    showFold: true,
    // 标签页样式
    theme: TabsThemeEnum.CARD,
  },

  // 动画配置
  transitionSetting: {
    //  是否开启切换动画
    // The disabled state will also disable pageLoadinng
    enable: true,

    // 动画名 Route basic switching animation
    basicTransition: RouterTransitionEnum.FADE_SIDE,

    // 是否打开页面切换loading
    // Only open when enable=true
    openPageLoading: true,

    //是否打开页面切换顶部进度条
    openNProgress: true,
  },

  // 是否开启KeepAlive缓存  开发时候最好关闭,不然每次都需要清除缓存
  openKeepAlive: true,

  // 自动锁屏时间，为0不锁屏。 单位分钟 默认1个小时
  lockTime: 0,

  // 显示面包屑
  showBreadCrumb: false,

  // 显示面包屑图标
  showBreadCrumbIcon: true,

  // 是否使用全局错误捕获
  useErrorHandle: false,

  // 是否开启回到顶部
  useOpenBackTop: true,

  // 是否可以嵌入iframe页面
  canEmbedIFramePage: true,

  // 切换界面的时候是否删除未关闭的message及notify
  closeMessageOnSwitch: true,

  // 切换界面的时候是否取消已经发送但是未响应的http请求。
  // 如果开启,想对单独接口覆盖。可以在单独接口设置
  removeAllHttpPending: false,
};

export default setting;
