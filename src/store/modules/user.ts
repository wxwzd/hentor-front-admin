// import type { UserInfo, LoginInfo } from '/#/store';
// import type { ErrorMessageMode } from '/#/axios';
// import { defineStore } from 'pinia';
// import { store } from '/@/store';
// import { RoleEnum } from '/@/enums/roleEnum';
// import { PageEnum } from '/@/enums/pageEnum';
// import {
//   ROLES_KEY,
//   TOKEN_KEY,
//   USER_INFO_KEY,
//   DB_DICT_DATA_KEY,
//   LOGIN_INFO_KEY,
// } from '/@/enums/cacheEnum';
// import { getAuthCache, setAuthCache } from '/@/utils/auth';
// import { GetUserInfoModel, LoginParams } from '/@/api/sys/model/userModel';
// import { doLogout, getUserInfo, loginApi } from '/@/api/sys/user';
// import { useI18n } from '/@/hooks/web/useI18n';
// import { useMessage } from '/@/hooks/web/useMessage';
// import { router } from '/@/router';
// import { usePermissionStore } from '/@/store/modules/permission';
// import { RouteRecordRaw } from 'vue-router';
// import { PAGE_NOT_FOUND_ROUTE } from '/@/router/routes/basic';
// import { isArray } from '/@/utils/is';
// import { h } from 'vue';

// interface UserState {
//   userInfo: Nullable<UserInfo>;
//   token?: string;
//   roleList: RoleEnum[];
//   sessionTimeout?: boolean;
//   lastUpdateTime: number;
//   dictItems?: [];
//   loginInfo?: Nullable<LoginInfo>;
// }

// export const useUserStore = defineStore({
//   id: 'app-user',
//   state: (): UserState => ({
//     // user info
//     userInfo: null,
//     // token
//     token: undefined,
//     // roleList
//     roleList: [],
//     // Whether the login expired
//     sessionTimeout: false,
//     // Last fetch time
//     lastUpdateTime: 0,
//     // 字典
//     dictItems: [],
//   }),
//   getters: {
//     getUserInfo(state): UserInfo {
//       return state.userInfo || getAuthCache<UserInfo>(USER_INFO_KEY) || {};
//     },
//     getToken(state): string {
//       return state.token || getAuthCache<string>(TOKEN_KEY);
//     },
//     getRoleList(state): RoleEnum[] {
//       return state.roleList.length > 0 ? state.roleList : getAuthCache<RoleEnum[]>(ROLES_KEY);
//     },
//     getSessionTimeout(state): boolean {
//       return !!state.sessionTimeout;
//     },
//     getLastUpdateTime(state): number {
//       return state.lastUpdateTime;
//     },
//   },
//   actions: {
//     setToken(info: string | undefined) {
//       this.token = info ? info : ''; // for null or undefined value
//       setAuthCache(TOKEN_KEY, info);
//     },
//     setRoleList(roleList: RoleEnum[]) {
//       this.roleList = roleList;
//       setAuthCache(ROLES_KEY, roleList);
//     },
//     setUserInfo(info: UserInfo | null) {
//       this.userInfo = info;
//       this.lastUpdateTime = new Date().getTime();
//       setAuthCache(USER_INFO_KEY, info);
//     },
//     setSessionTimeout(flag: boolean) {
//       this.sessionTimeout = flag;
//     },
//     setAllDictItems(dictItems) {
//       this.dictItems = dictItems;
//       setAuthCache(DB_DICT_DATA_KEY, dictItems);
//     },
//     resetState() {
//       this.userInfo = null;
//       this.token = '';
//       this.roleList = [];
//       this.sessionTimeout = false;
//     },
//     setLoginInfo(info: LoginInfo | null) {
//       this.loginInfo = info;
//       setAuthCache(LOGIN_INFO_KEY, info);
//     },
//     /**
//      * @description: login
//      */
//     async login(
//       params: LoginParams & {
//         goHome?: boolean;
//         mode?: ErrorMessageMode;
//       },
//     ): Promise<GetUserInfoModel | null> {
//       try {
//         const { goHome = true, mode, ...loginParams } = params;
//         const data = await loginApi(loginParams, mode);
//         const { token } = data;
//         // save token
//         this.setToken(token);
//         return this.afterLoginAction(goHome);
//       } catch (error) {
//         return Promise.reject(error);
//       }
//     },
//     // async afterLoginAction(goHome?: boolean): Promise<GetUserInfoModel | null> {
//     //   if (!this.getToken) return null;
//     //   // get user info
//     //   const userInfo = await this.getUserInfoAction();

//     //   const sessionTimeout = this.sessionTimeout;
//     //   if (sessionTimeout) {
//     //     this.setSessionTimeout(false);
//     //   } else {
//     //     const permissionStore = usePermissionStore();
//     //     if (!permissionStore.isDynamicAddedRoute) {
//     //       const routes = await permissionStore.buildRoutesAction();
//     //       routes.forEach((route) => {
//     //         router.addRoute(route as unknown as RouteRecordRaw);
//     //       });
//     //       router.addRoute(PAGE_NOT_FOUND_ROUTE as unknown as RouteRecordRaw);
//     //       permissionStore.setDynamicAddedRoute(true);
//     //     }
//     //     goHome && (await router.replace(userInfo?.homePath || PageEnum.BASE_HOME));
//     //   }
//     //   return userInfo;
//     // },
//     async afterLoginAction(goHome?: boolean, data?: any): Promise<any | null> {
//       if (!this.getToken) return null;
//       //获取用户信息
//       const userInfo = await this.getUserInfoAction();
//       const sessionTimeout = this.sessionTimeout;
//       console.log('sessionTimeout', sessionTimeout);
//       if (sessionTimeout) {
//         this.setSessionTimeout(false);
//       } else {
//         const permissionStore = usePermissionStore();
//         if (!permissionStore.isDynamicAddedRoute) {
//           const routes = await permissionStore.buildRoutesAction();
//           routes.forEach((route) => {
//             router.addRoute(route as unknown as RouteRecordRaw);
//           });
//           router.addRoute(PAGE_NOT_FOUND_ROUTE as unknown as RouteRecordRaw);
//           permissionStore.setDynamicAddedRoute(true);
//         }
//         await this.setLoginInfo({ ...data, isLogin: true });
//         goHome && (await router.replace((userInfo && userInfo.homePath) || PageEnum.BASE_HOME));
//       }
//       return data;
//     },
//     async getUserInfoAction(): Promise<UserInfo | null> {
//       if (!this.getToken) return null;
//       const { userInfo, sysAllDictItems } = await getUserInfo();
//       const { roles = [] } = userInfo;
//       if (isArray(roles)) {
//         const roleList = roles.map((item) => item.value) as RoleEnum[];
//         this.setRoleList(roleList);
//       } else {
//         userInfo.roles = [];
//         this.setRoleList([]);
//       }
//       this.setUserInfo(userInfo);
//       /**
//        * 添加字典信息到缓存
//        */
//       if (sysAllDictItems) {
//         this.setAllDictItems(sysAllDictItems);
//       }
//       return userInfo;
//     },
//     /**
//      * @description: logout
//      */
//     async logout(goLogin = false) {
//       if (this.getToken) {
//         try {
//           await doLogout();
//         } catch {
//           console.log('注销Token失败');
//         }
//       }
//       this.setToken(undefined);
//       this.setSessionTimeout(false);
//       this.setUserInfo(null);
//       goLogin && router.push(PageEnum.BASE_LOGIN);
//     },

//     /**
//      * @description: Confirm before logging out
//      */
//     confirmLoginOut() {
//       const { createConfirm } = useMessage();
//       const { t } = useI18n();
//       createConfirm({
//         iconType: 'warning',
//         title: () => h('span', t('sys.app.logoutTip')),
//         content: () => h('span', t('sys.app.logoutMessage')),
//         onOk: async () => {
//           await this.logout(true);
//         },
//       });
//     },
//   },
// });

// // Need to be used outside the setup
// export function useUserStoreWithOut() {
//   return useUserStore(store);
// }

import type { UserInfo, LoginInfo } from '/#/store';
import type { ErrorMessageMode } from '/#/axios';
import { defineStore } from 'pinia';
import { store } from '/@/store';
import { RoleEnum } from '/@/enums/roleEnum';
import { PageEnum } from '/@/enums/pageEnum';
import {
  ROLES_KEY,
  TOKEN_KEY,
  USER_INFO_KEY,
  LOGIN_INFO_KEY,
  DB_DICT_DATA_KEY,
  TENANT_ID,
} from '/@/enums/cacheEnum';
import { getAuthCache, setAuthCache } from '/@/utils/auth';
import { GetUserInfoModel, LoginParams } from '/@/api/sys/model/userModel';
import { doLogout, getUserInfo, loginApi } from '/@/api/sys/user';
import { useI18n } from '/@/hooks/web/useI18n';
import { useMessage } from '/@/hooks/web/useMessage';
import { router } from '/@/router';
import { usePermissionStore } from '/@/store/modules/permission';
import { RouteRecordRaw } from 'vue-router';
import { PAGE_NOT_FOUND_ROUTE } from '/@/router/routes/basic';
import { isArray } from '/@/utils/is';
import { useGlobSetting } from '/@/hooks/setting';

interface UserState {
  userInfo: Nullable<UserInfo>;
  token?: string;
  roleList: RoleEnum[];
  dictItems?: [];
  sessionTimeout?: boolean;
  lastUpdateTime: number;
  tenantid?: string | number;
  loginInfo?: Nullable<LoginInfo>;
}

export const useUserStore = defineStore({
  id: 'app-user',
  state: (): UserState => ({
    // 用户信息
    userInfo: null,
    // token
    token: undefined,
    // 角色列表
    roleList: [],
    // 字典
    dictItems: [],
    // session过期时间
    sessionTimeout: false,
    // Last fetch time
    lastUpdateTime: 0,
    //租户id
    tenantid: '',
    //登录返回信息
    loginInfo: null,
  }),
  getters: {
    getUserInfo(): UserInfo {
      return this.userInfo || getAuthCache<UserInfo>(USER_INFO_KEY) || {};
    },
    getLoginInfo(): LoginInfo {
      return this.loginInfo || getAuthCache<LoginInfo>(LOGIN_INFO_KEY) || {};
    },
    getToken(): string {
      return this.token || getAuthCache<string>(TOKEN_KEY);
    },
    getAllDictItems(): [] {
      return this.dictItems || getAuthCache(DB_DICT_DATA_KEY);
    },
    getRoleList(): RoleEnum[] {
      return this.roleList.length > 0 ? this.roleList : getAuthCache<RoleEnum[]>(ROLES_KEY);
    },
    getSessionTimeout(): boolean {
      return !!this.sessionTimeout;
    },
    getLastUpdateTime(): number {
      return this.lastUpdateTime;
    },
    getTenant(): string | number {
      return this.tenantid || getAuthCache<string | number>(TENANT_ID);
    },
  },
  actions: {
    setToken(info: string | undefined) {
      this.token = info ? info : ''; // for null or undefined value
      setAuthCache(TOKEN_KEY, info);
    },
    setRoleList(roleList: RoleEnum[]) {
      this.roleList = roleList;
      setAuthCache(ROLES_KEY, roleList);
    },
    setUserInfo(info: UserInfo | null) {
      this.userInfo = info;
      this.lastUpdateTime = new Date().getTime();
      setAuthCache(USER_INFO_KEY, info);
    },
    setLoginInfo(info: LoginInfo | null) {
      this.loginInfo = info;
      setAuthCache(LOGIN_INFO_KEY, info);
    },
    setAllDictItems(dictItems) {
      this.dictItems = dictItems;
      setAuthCache(DB_DICT_DATA_KEY, dictItems);
    },
    setTenant(id) {
      this.tenantid = id;
      setAuthCache(TENANT_ID, id);
    },
    setSessionTimeout(flag: boolean) {
      this.sessionTimeout = flag;
    },
    resetState() {
      this.userInfo = null;
      this.dictItems = [];
      this.token = '';
      this.roleList = [];
      this.sessionTimeout = false;
    },
    /**
     * 登录事件
     */
    async login(
      params: LoginParams & {
        goHome?: boolean;
        mode?: ErrorMessageMode;
      },
    ): Promise<GetUserInfoModel | null> {
      try {
        const { goHome = true, mode, ...loginParams } = params;
        const data = await loginApi(loginParams, mode);
        const { token, userInfo } = data;
        // save token
        this.setToken(token);
        this.setTenant(userInfo.loginTenantId);
        return this.afterLoginAction(goHome, data);
      } catch (error) {
        return Promise.reject(error);
      }
    },

    /**
     * 登录完成处理
     * @param goHome
     */
    async afterLoginAction(goHome?: boolean, data?: any): Promise<any | null> {
      if (!this.getToken) return null;
      //获取用户信息
      const userInfo = await this.getUserInfoAction();
      const sessionTimeout = this.sessionTimeout;
      if (sessionTimeout) {
        this.setSessionTimeout(false);
      } else {
        const permissionStore = usePermissionStore();
        if (!permissionStore.isDynamicAddedRoute) {
          const routes = await permissionStore.buildRoutesAction();
          routes.forEach((route) => {
            router.addRoute(route as unknown as RouteRecordRaw);
          });
          router.addRoute(PAGE_NOT_FOUND_ROUTE as unknown as RouteRecordRaw);
          permissionStore.setDynamicAddedRoute(true);
        }
        await this.setLoginInfo({ ...data, isLogin: true });
        goHome && (await router.replace((userInfo && userInfo.homePath) || PageEnum.BASE_HOME));
      }
      return data;
    },

    /**
     * 获取用户信息
     */
    async getUserInfoAction(): Promise<UserInfo | null> {
      if (!this.getToken) {
        return null;
      }
      const { userInfo, sysAllDictItems } = await getUserInfo();
      if (userInfo) {
        const { roles = [] } = userInfo;
        if (isArray(roles)) {
          const roleList = roles.map((item) => item.value) as RoleEnum[];
          this.setRoleList(roleList);
        } else {
          userInfo.roles = [];
          this.setRoleList([]);
        }
        this.setUserInfo(userInfo);
      }
      /**
       * 添加字典信息到缓存
       * @updateBy:lsq
       * @updateDate:2021-09-08
       */
      if (sysAllDictItems) {
        this.setAllDictItems(sysAllDictItems);
      }
      return userInfo;
    },
    /**
     * 退出登录
     */
    async logout(goLogin = false) {
      if (this.getToken) {
        try {
          await doLogout();
        } catch {
          console.log('注销Token失败');
        }
      }

      // //update-begin-author:taoyan date:2022-5-5 for: src/layouts/default/header/index.vue showLoginSelect方法 获取tenantId 退出登录后再次登录依然能获取到值，没有清空
      // let username:any = this.userInfo && this.userInfo.username;
      // if(username){
      //   removeAuthCache(username)
      // }
      // //update-end-author:taoyan date:2022-5-5 for: src/layouts/default/header/index.vue showLoginSelect方法 获取tenantId 退出登录后再次登录依然能获取到值，没有清空

      this.setToken('');
      setAuthCache(TOKEN_KEY, null);
      this.setSessionTimeout(false);
      this.setUserInfo(null);
      this.setLoginInfo(null);
      this.setTenant(null);

      //如果开启单点登录,则跳转到单点统一登录中心
      const openSso = useGlobSetting().openSso;
      if (openSso == 'true') {
        await useSso().ssoLoginOut();
      }

      goLogin && (await router.push(PageEnum.BASE_LOGIN));
    },

    /**
     * 退出询问
     */
    confirmLoginOut() {
      const { createConfirm } = useMessage();
      const { t } = useI18n();
      createConfirm({
        iconType: 'warning',
        title: t('sys.app.logoutTip'),
        content: t('sys.app.logoutMessage'),
        onOk: async () => {
          await this.logout(true);
        },
      });
    },
  },
});

// Need to be used outside the setup
export function useUserStoreWithOut() {
  return useUserStore(store);
}
