import { getAuthCache, setAuthCache, clearAuthCache } from '/@/utils/auth';
import { DB_DICT_DATA_KEY } from '/@/enums/cacheEnum';
import { defHttp } from '/@/utils/http/axios';
import { refreshCache, queryAllDictItems } from '/@/api/system/dict/dict.api';
/**
 * 从缓存中获取字典配置
 * @param code
 */
export const getDictItemsByCode = (code) => {
  if (getAuthCache(DB_DICT_DATA_KEY) && getAuthCache(DB_DICT_DATA_KEY)[code]) {
    return getAuthCache(DB_DICT_DATA_KEY)[code];
  }
};
/**
 * 获取字典数组
 * @param dictCode 字典Code
 * @return List<Map>
 */
export const initDictOptions = (code) => {
  //1.优先从缓存中读取字典配置
  if (getDictItemsByCode(code)) {
    return new Promise((resolve, reject) => {
      resolve(getDictItemsByCode(code));
    });
  }
  //2.获取字典数组
  //update-begin-author:taoyan date:2022-6-21 for: 字典数据请求前将参数编码处理，但是不能直接编码，因为可能之前已经编码过了
  if (code.indexOf(',') > 0 && code.indexOf(' ') > 0) {
    // 编码后类似sys_user%20where%20username%20like%20xxx' 是不包含空格的,这里判断如果有空格和逗号说明需要编码处理
    code = encodeURI(code);
  }
  //update-end-author:taoyan date:2022-6-21 for: 字典数据请求前将参数编码处理，但是不能直接编码，因为可能之前已经编码过了
  return defHttp.get({ url: `/sys/dict/getDictItems/${code}` });
};
// 清除缓存
export const clearCache = () => {
  const result = refreshCache();
  if (result.success) {
    const res = queryAllDictItems();
    clearAuthCache(DB_DICT_DATA_KEY);
    setAuthCache(DB_DICT_DATA_KEY, res.result);
  }
};
//初始化缓存
export const initAllDictItems = () => {
  const res = queryAllDictItems();
  setAuthCache(DB_DICT_DATA_KEY, res.result);
};
/**
 * 获取字典数组
 * @param code 字典Code
 * @param params 查询参数
 * @param options 查询配置
 * @return List<Map>
 */
export const ajaxGetDictItems = (code, params, options?) =>
  defHttp.get({ url: `/sys/dict/getDictItems/${code}`, params }, options);

export function filterDictText(dictOptions, text) {
  if (text != null && Array.isArray(dictOptions)) {
    const result = [];
    // 允许多个逗号分隔，允许传数组对象
    let splitText;
    if (Array.isArray(text)) {
      splitText = text;
    } else {
      splitText = text.toString().trim().split(',');
    }
    for (const txt of splitText) {
      let dictText = txt;
      for (const dictItem of dictOptions) {
        if (txt.toString() === dictItem.value.toString()) {
          dictText = dictItem.text || dictItem.title || dictItem.label;
          break;
        }
      }
      result.push(dictText);
    }
    return result.join(',');
  }
  return text;
}
