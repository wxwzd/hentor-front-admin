import { BasicColumn, FormSchema } from '/@/components/Table';
import { VxeFormItemProps, VxeGridPropTypes } from '/@/components/VxeTable';

export const columns: BasicColumn[] = [
  {
    title: '表名',
    dataIndex: 'tableName',
    width: 200,
  },
  {
    title: '表描述',
    dataIndex: 'tableDescription',
    width: 180,
  },

  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'tableName',
    label: '表名',
    component: 'Input',
    colProps: { span: 8 },
  },
];
export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    show: false,
  },
  {
    field: 'tableName',
    label: '表名',
    required: true,
    component: 'Input',
    rules: [
      {
        pattern: /^[a-zA-Z](?=.*_)[a-zA-Z0-9_]*$/,
        message: '只能以字母加下划线开头，并且只能包含字母、数字和下划线',
      },
    ],
  },
  {
    field: 'tableDescription',
    label: '表描述',
    required: true,
    component: 'Input',
  },
];

export const codeGenformSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    show: false,
  },
  {
    field: 'tableName',
    label: '表名',
    required: true,
    component: 'Input',
    dynamicDisabled: true,
    rules: [
      {
        pattern: /^[a-zA-Z](?=.*_)[a-zA-Z0-9_]*$/,
        message: '只能以字母加下划线开头，并且只能包含字母、数字和下划线',
      },
    ],
  },
  {
    field: 'packagePath',
    label: '包名',
    required: true,
    component: 'Input',
  },
];

export const codeGenDowformSchema: FormSchema[] = [
  {
    field: 'fileKey',
    label: 'fileKey',
    component: 'Input',
    show: false,
  },
];

export const detailColumns: VxeGridPropTypes.Columns = [
  {
    title: '字段名',
    field: 'columnName',
    width: '200',
  },
  {
    title: '字段类型',
    field: 'columnType',
    width: '180',
  },

  {
    title: '字段长度',
    field: 'columnLength',
    width: '180',
  },
  {
    title: '小数点',
    field: 'columnPoint',
    width: '180',
  },
  {
    title: '默认值',
    field: 'columnDef',
    width: '180',
  },
  {
    title: '字段备注',
    field: 'columnDesc',
    width: '180',
  },
];
