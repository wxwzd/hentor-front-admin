import { BasicColumn, FormSchema } from '/@/components/Table';
import { dictItemCheck } from '/@/api/system/dict/dict.api';

export const columns: BasicColumn[] = [
  {
    title: '字典名称',
    dataIndex: 'dictName',
    align: 'left',
  },
  {
    title: '字典编码',
    dataIndex: 'dictCode',
    width: 150,
  },

  {
    title: '描述',
    dataIndex: 'description',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'dictName',
    label: '字典名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'dictCode',
    label: '字典编码',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'id',
    component: 'Input',
    show: false,
  },
  {
    field: 'dictName',
    label: '字典名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'dictCode',
    label: '字典编码',
    required: true,
    component: 'Input',
  },
  {
    field: 'description',
    label: '描述',
    component: 'Input',
  },
];

export const dictItemColumns: BasicColumn[] = [
  {
    title: '名称',
    dataIndex: 'itemText',
    width: 80,
  },
  {
    title: '数据值',
    dataIndex: 'itemValue',
    width: 80,
  },
];

export const dictItemSearchFormSchema: FormSchema[] = [
  {
    label: '名称',
    field: 'itemText',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    label: '数据值',
    field: 'itemValue',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    label: '状态',
    field: 'status',
    component: 'DictSelectTag',
    componentProps: {
      dictCode: 'dict_item_status',
      stringToNumber: true,
      showChooseOption: false,
    },
    colProps: { span: 8 },
  },
];

export const itemFormSchema: FormSchema[] = [
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
  {
    label: '名称',
    field: 'itemText',
    required: true,
    component: 'Input',
  },
  {
    label: '数据值',
    field: 'itemValue',
    component: 'Input',
    dynamicRules: ({ values, model }) => {
      return [
        {
          required: true,
          validator: (_, value) => {
            if (!value) {
              return Promise.reject('请输入数据值');
            }
            if (new RegExp("[`~!@#$^&*()=|{}'.<>《》/?！￥（）—【】‘；：”“。，、？]").test(value)) {
              return Promise.reject('数据值不能包含特殊字符！');
            }
            return new Promise<void>((resolve, reject) => {
              const params = {
                dictId: values.dictId,
                id: model.id,
                itemValue: value,
              };
              dictItemCheck(params)
                .then((res) => {
                  res.success ? resolve() : reject(res.message || '校验失败');
                })
                .catch((err) => {
                  reject(err.message || '验证失败');
                });
            });
          },
        },
      ];
    },
  },
  {
    label: '描述',
    field: 'description',
    component: 'Input',
  },
  {
    field: 'sortOrder',
    label: '排序',
    component: 'InputNumber',
    defaultValue: 1,
  },
  {
    label: '状态',
    field: 'status',
    component: 'DictSelectTag',
    componentProps: {
      dictCode: 'dict_item_status',
      stringToNumber: true,
      showChooseOption: false,
    },
    colProps: { span: 8 },
  },
];
